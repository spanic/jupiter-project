import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {Md5} from 'ts-md5';
import {Subscription} from 'rxjs';

import {UsersService} from '../users.service';
import {CookieService} from "ngx-cookie-service";

import {ClassUsers} from '../ClassUsers';
import {ClassNewUser} from "../ClassNewUser";

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css', '../app.component.css']
})
export class SignUpComponent implements OnInit, OnDestroy {

  user: ClassUsers;
  newUser: ClassNewUser;

  subscription: Subscription;

  allFields: boolean = true;
  message: string;

  @ViewChild('inputFullName')
  inputFullNameElement: ElementRef;

  @ViewChild('inputPassword')
  inputPasswordElement: ElementRef;

  @ViewChild('inputEmail')
  inputEmailElement: ElementRef;
  userLoginText: any;
  inputEmailtext: any;
  inputPasswordtext: any;

  constructor(
    public  router: Router,
    public  usersService: UsersService,
    public  cookie: CookieService
  ) {
  }

  ngOnInit() {
    if (!!this.cookie.get('userid')) {
      this.router.navigate([`/profile/${Number(this.cookie.get('userid'))}`]);
    }
  }

  ngOnDestroy() {
    !!this.subscription && this.subscription.unsubscribe()
  }

  addUser(login: string, email: string, password: string): void {
    !!this.usersService.user && this.usersService.setUndefinedLocalUser();
    if (!login && !email && !password) {
      return;
    }
    this.subscription = this.usersService.addUser({email, login, password} as ClassUsers)
      .subscribe(user => this.newUser = user,
        (error: any) => {
          this.message = error.error.response;
          if (this.message == 'login exist') {
            this.message = "Login already exists";
          }
          if (this.message == 'email exist') {
            this.message = "Email already exists"
          }
        },
        () => {
          this.usersService.setLocalUser();
          this.user = this.newUser.user;
          let date = new Date(0);
          document.cookie = "userid=; path=/; expires=" + date.toUTCString();
          document.cookie = "token=; path=/; expires=" + date.toUTCString();
          document.cookie = `userid=${this.user.id}; path =/`;
          document.cookie = `token=${this.newUser.token}; path =/`;

          this.router.navigate([`/profile/${this.user.id}`]);
        });
  }

  onSignUpClick() {
    this.message = undefined;
    if (this.inputFullNameElement.nativeElement.value == "" || this.inputPasswordElement.nativeElement.value == "" || this.inputEmailElement.nativeElement.value == "") {
      this.allFields = false;
      this.message = "All fields should be filled";
    } else {
      this.allFields = true;
      const md5 = new Md5();
      let hash = md5.appendStr(this.inputPasswordElement.nativeElement.value).end();
      hash = hash.toString();
      this.addUser(this.inputFullNameElement.nativeElement.value, this.inputEmailElement.nativeElement.value, hash);
    }
  }

}
