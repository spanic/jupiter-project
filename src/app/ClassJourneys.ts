export class ClassJourneys {
  journeyId: number;
  numberId: number;
  newTariffId: number;
  number: string;
  previousProvider: string;
  previousTarif: string;
  nextProvider: string;
  nextTarif: string;
  date: Date;
}
