import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {Subscription} from "rxjs";

import {UsersService} from "../users.service";
import {CookieService} from "ngx-cookie-service";

import {ClassUsers} from "../ClassUsers";

@Component({
  selector: 'app-phone-numbers',
  templateUrl: './phone-numbers.component.html',
  styleUrls: ['./phone-numbers.component.css', '../app.component.css']
})
export class PhoneNumbersComponent implements OnInit, OnDestroy {

  private user: ClassUsers;
  private subscription: Subscription;

  constructor(
    private userService: UsersService,
    private cookie: CookieService,
    private router: Router
  ) {
  }

  ngOnInit() {
    if (!!this.cookie.get('userid')) {
      this.subscription = this.userService.getUser(Number(this.cookie.get('userid'))).subscribe({
        next: user => this.user = user,
        complete: () => {
          if (!!this.user) {
            if (!this.user.birthDate) this.router.navigate([`/profile/${this.user.id}`]);
          }
        }})
    } else {
      this.router.navigate(['/log-in']);
    }
  }

  ngOnDestroy(): void {
    !!this.subscription && this.subscription.unsubscribe();
  }

}
