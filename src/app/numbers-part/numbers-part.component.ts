import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';

import {NumberService} from '../number.service';
import {OfferingsService} from '../offerings.service';
import {CookieService} from "ngx-cookie-service";

import {ClassOfferings} from '../ClassOfferings';
import { ClassNumber} from '../ClassNumber';

@Component({
  selector: 'app-numbers-part',
  templateUrl: './numbers-part.component.html',
  styleUrls: ['./numbers-part.component.css', '../app.component.css']
})
export class NumbersPartComponent implements OnInit, OnDestroy {

  @ViewChild('InfoDiv')
  InfoDivElement: ElementRef;

  @ViewChild('InfoBtn')
  InfoBtnElement: ElementRef;

  numbers: ClassNumber[];
  number: ClassNumber;
  offering: ClassOfferings[];
  subscriptions: Subscription[] = [];

  constructor(
    private numbersService: NumberService,
    private offeringsService: OfferingsService,
    private route: ActivatedRoute,
    private cookie: CookieService
  ) { }

  ngOnInit() {
    if(!!this.cookie.get('userid')){
      this.getNumbers();
    }
  }

  ngOnDestroy(){
    this.subscriptions.forEach(
      (subscription) => !!subscription && subscription.unsubscribe());
    this.subscriptions = [];
  }

  getNumbers(): void {
    const id = JSON.parse(this.cookie.get('userid'));
    this.subscriptions.push(this.numbersService.getNumbers(id)
      .subscribe(numbers => this.numbers = numbers));
  }

  getOffering(id: number): void {
    this.subscriptions.push(this.offeringsService.getOfferings(id)
      .subscribe(offering => this.offering= offering));
  }
}
