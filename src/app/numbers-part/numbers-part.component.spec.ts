import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NumbersPartComponent } from './numbers-part.component';

describe('NumbersPartComponent', () => {
  let component: NumbersPartComponent;
  let fixture: ComponentFixture<NumbersPartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NumbersPartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumbersPartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
