import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import {MyHttpService} from "./my-http.service";
import {HandleErrorsService} from './handle-errors.service';

import {ClassOfferings} from './ClassOfferings';
import {ClassTariff} from "./ClassTariff";

@Injectable({
  providedIn: 'root'
})
export class OfferingsService {

  constructor(
    private http: MyHttpService,
    private handleErrors: HandleErrorsService
  ) { }

  getTariffs(regionId: number, providerId: number): Observable<ClassTariff[]> {
    const url = `http://138.197.187.9:8080/tarif/price/${regionId}/${providerId}`;
    return this.http.get<ClassTariff[]>(url)
      .pipe(
        catchError(this.handleErrors.handleError<ClassTariff[]>('getTariffs', []))
      );
  }

  getOfferings(id: number): Observable<ClassOfferings[]> {
    const url = `http://138.197.187.9:8080/tariffoffers/offers/${id}`;
    return this.http.get<ClassOfferings[]>(url)
      .pipe(
        catchError(this.handleErrors.handleError<ClassOfferings[]>(`getOfferings`))
      );
  }
}
