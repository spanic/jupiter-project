import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import {MyHttpService} from "./my-http.service";
import {HandleErrorsService} from './handle-errors.service';

import {ClassCountries} from './ClassCountries';
import {ClassRegion} from "./ClassRegion";

@Injectable({
  providedIn: 'root'
})
export class CountriesService {

  private countriesUrl = 'http://138.197.187.9:8080/country';

  constructor(
    private http: MyHttpService,
    private handleErrors: HandleErrorsService
  ) { }

  getCountries(): Observable<ClassCountries[]> {
    const url = `${this.countriesUrl}/all`;
    return this.http.get<ClassCountries[]>(url)
      .pipe(
        catchError(this.handleErrors.handleError<ClassCountries[]>('getCountries', []))
      );
}
  getRegions(id: number): Observable<ClassRegion[]> {
    const url = `${this.countriesUrl}/${id}`;
    return this.http.get<ClassRegion[]>(url)
      .pipe(
        catchError(this.handleErrors.handleError<ClassRegion[]>('getRegions', []))
      );
  }
}
