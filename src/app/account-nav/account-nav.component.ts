import { Component, OnInit, OnDestroy } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';

import {CookieService} from 'ngx-cookie-service';
import {UsersService} from '../users.service';
import {JourneysService} from '../journeys.service';

import {ClassUsers} from '../ClassUsers';
import {ClassCompletedJourneys} from '../ClassCompletedJourneys';

@Component({
  selector: 'app-account-nav',
  templateUrl: './account-nav.component.html',
  styleUrls: ['./account-nav.component.css']
})
export class AccountNavComponent implements OnInit, OnDestroy {

   path : string;
   user: ClassUsers;
   regexpProfile: RegExp = /^[\w\W]*(profile)[\w\W]*$/;
   regexpPhoneNumbers: RegExp = /^[\w\W]*(phone-numbers)[\w\W]*$/;
   regexpJourneys: RegExp = /^[\w\W]*(journeys)[\w\W]*$/;
   journeys: ClassCompletedJourneys[];
   subscriptions: Subscription[] = [];
   id: number;

  constructor(
    public usersService: UsersService,
    public route: ActivatedRoute,
    public cookie: CookieService,
    public journeysService: JourneysService
  ) { }

  ngOnInit() {
    if(!!this.cookie.get('userid')) {
      this.id = JSON.parse(this.cookie.get('userid'))
      this.getUser();
      this.getCompletedJourneys();
      this.path = document.location.pathname.substring(1);
    }
  }

  ngOnDestroy(){
    this.subscriptions.forEach(
      (subscription) => subscription.unsubscribe());
    this.subscriptions = [];
  }

  getUser(): void {
    this.subscriptions.push(this.usersService.getUser(this.id)
      .subscribe(user => this.user = user));
  }

  getCompletedJourneys(): void {
    this.subscriptions.push(this.journeysService.getCompletedJourneys(this.id)
    // @ts-ignore
      .subscribe(journeys => this.journeys = journeys, (e)=>{}, ()=> {
          if (this.journeys && this.journeys.length > 0) {
            // @ts-ignore
            document.styleSheets[0].addRule('#jrn.special:after', 'visibility: visible;');
            // @ts-ignore
            document.styleSheets[0].addRule('#jrn.special:after', 'content: "' + this.journeys.length + '";');
          }else{
            // @ts-ignore
            document.styleSheets[0].addRule('#jrn.special:after', 'visibility: hidden;');
          }
      }
        ));
  }
}
