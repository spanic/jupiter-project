export class ClassUsers {
  id: number;
  surname: string;
  name: string;
  patronymic: string;
  login: string;
  birthDate: string;
  passport: string;
  email: string;
  password: string;
}
