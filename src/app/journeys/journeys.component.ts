import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {Subscription} from "rxjs";

import {UsersService} from "../users.service";
import {CookieService} from "ngx-cookie-service";

import {ClassUsers} from "../ClassUsers";

@Component({
  selector: 'app-journeys',
  templateUrl: './journeys.component.html',
  styleUrls: ['./journeys.component.css', '../app.component.css']
})
export class JourneysComponent implements OnInit, OnDestroy {

  user: ClassUsers;
  id: number;
  subscription: Subscription;

  constructor(
    private userService: UsersService,
    private cookie: CookieService,
    private router: Router
  ) { }

  ngOnInit() {
    if(!!this.cookie.get('userid')){
      this.id = Number(this.cookie.get('userid'));
      this.subscription = this.userService.getUser(this.id).subscribe(user => this.user = user, (e): void => {},
        () => {
          if(!!this.user) {
            if (!this.user.birthDate) this.router.navigate([`/profile/${this.user.id}`]);
          }
        })
    }else{
      this.router.navigate(['/log-in']);
    }
  }

  ngOnDestroy(): void {
    !!this.subscription && this.subscription.unsubscribe();
  }
}
