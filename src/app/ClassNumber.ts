export class ClassNumber {
  userId: number
  id: number;
  number: string;
  country: string;
  region: string;
  provider: string;
  tariffId: number;
  tariff: string;
  tarifPrice: string;
  balance: number;
}
