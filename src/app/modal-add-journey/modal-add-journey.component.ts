import {Component, OnInit, OnDestroy, Output, EventEmitter} from '@angular/core';
import {Subscription} from 'rxjs';
import {ActivatedRoute} from '@angular/router';

import {ProvidersService} from "../providers.service";
import {OfferingsService} from "../offerings.service";
import {CountriesService} from '../countries.service';
import {TariffsService} from "../tariffs.service";
import {JourneysService} from '../journeys.service';
import {CookieService} from "ngx-cookie-service";

import {ClassCompletedJourneys} from '../ClassCompletedJourneys';
import {ClassTariffOfferings} from '../ClassTariffOfferings';
import {ClassRegion} from "../ClassRegion";
import {ClassProviders} from "../ClassProviders";
import {ClassTariff} from "../ClassTariff";
import {ClassNumber} from '../ClassNumber';

declare var $: any;

@Component({
  selector: 'app-modal-add-journey',
  templateUrl: './modal-add-journey.component.html',
  styleUrls: ['./modal-add-journey.component.css', '../app.component.css']
})
export class ModalAddJourneyComponent implements OnInit, OnDestroy {

  @Output() public addJourneys = new EventEmitter;

  numbers: ClassNumber[];
  number: ClassNumber;
  regions: ClassRegion[];
  providers: ClassProviders[];
  tariffs: ClassTariff[];
  offerings: ClassTariffOfferings[];
  journeys: ClassCompletedJourneys[];

  subscriptions: Subscription[] = [];

  selectedRegionId: number;
  selectedProviderId: number;
  selectedTariffId: number;
  selectedNumber: number;

  constructor(
    private countriesService: CountriesService,
    private providersService: ProvidersService,
    private offeringsService: OfferingsService,
    private journeysService: JourneysService,
    private tariffsService: TariffsService,
    private route: ActivatedRoute,
    private cookie: CookieService
  ) {
  }

  ngOnInit() {
    if (!!this.cookie.get('userid')) {
      this.getCompletedNumbers();
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(
      (subscription) => !!subscription && subscription.unsubscribe());
    this.subscriptions = [];
  }

  getCompletedNumbers(): void {
    const id = JSON.parse(this.cookie.get('userid'));
    this.subscriptions.push(this.journeysService.getCompletedNumbers(id)
      .subscribe(journeys => this.journeys = journeys));
  }

  getRegions(id: number): void {
    this.subscriptions.push(this.countriesService.getRegions(id)
      .subscribe(regions => this.regions = regions));
    this.selectedRegionId = undefined;
    this.selectedProviderId = undefined;
    this.providers = [];
    this.selectedTariffId = undefined;
    this.tariffs = [];
  }

  getProviders(): void {
    this.subscriptions.push(this.providersService.getProviders(this.selectedRegionId)
      .subscribe(providers => this.providers = providers));
    this.selectedProviderId = undefined;
    this.selectedTariffId = undefined;
    this.tariffs = [];
  }

  getTariffs(): void {
    this.subscriptions.push(this.offeringsService.getTariffs(this.selectedRegionId, this.selectedProviderId)
      .subscribe(tariffs => this.tariffs = tariffs));
    this.selectedTariffId = undefined;
  }

  getTariffOfferings(): void {
    this.subscriptions.push(this.tariffsService.getTariffOfferings(this.selectedTariffId)
      .subscribe(offerings => this.offerings = offerings));
  }

  addJourney(numberId: number, tariffId: number): void {
    $('#ModalAddJourney').modal('hide');
    if (!numberId || !tariffId) {
      return;
    }
    this.subscriptions.push(this.journeysService.addJourney(numberId, tariffId)
      .subscribe({
        complete: () => {
          this.addJourneys.emit();
        }
      }));
  }
}
