import {ClassTask} from './ClassTask';

export class ClassTaskAndName {
  taskName: string;
  journeyTask: ClassTask;
}
