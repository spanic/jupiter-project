import {Component, ElementRef, EventEmitter, OnInit, OnDestroy, Output, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';

import {UsersService} from '../users.service';
import {CookieService} from "ngx-cookie-service";

import {ClassUsers} from '../ClassUsers';

declare var $: any;

@Component({
  selector: 'app-modal-edit',
  templateUrl: './modal-edit.component.html',
  styleUrls: ['./modal-edit.component.css', '../app.component.css']
})
export class ModalEditComponent implements OnInit, OnDestroy {

  @ViewChild('inputLogin')
  inputLoginElement: ElementRef;

  @ViewChild('inputEmail')
  inputEmailElement: ElementRef;

  @ViewChild('inputBday')
  inputBdayElement: ElementRef;

  @ViewChild('inputPassport')
  inputPassportElement: ElementRef;

  @ViewChild('inputName')
  inputNameElement: ElementRef;

  @ViewChild('inputSurname')
  inputSurnameElement: ElementRef;

  user: ClassUsers;
  tmpUser: ClassUsers;
  subscription: Subscription;
  regexpPassport: RegExp = /^\d{4}\s\d{6}$/;
  regexpEmail: RegExp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  _message: string;

  @Output() public updateUser = new EventEmitter;

  constructor(
    private route: ActivatedRoute,
    private usersService: UsersService,
    private cookie: CookieService
  ) {
  }

  emptyLogin: boolean;
  emptyEmail: boolean;
  emptyBday: boolean;
  emptyPassport: boolean;
  emptyName: boolean;
  emptySurname: boolean;

  ngOnInit() {
  }

  get message() {
    return this._message;
  }

  set message(str: string) {
    this._message = str;
  }

  ngOnDestroy() {
    !!this.subscription && this.subscription.unsubscribe()
  }

  getUser(): void {
    const id = JSON.parse(this.cookie.get('userid'));
    this.emptyPassport = this.emptySurname = this.emptyName = this.emptyBday = this.emptyEmail = this.emptyLogin = false;
    //c каждым вызовом модального окна обнуляем юзера и получаем свежего с сервера
    // иначе нам вернется локальный юзер из компонента и мы будем обновлять его, используя ngModel в html разметке
    // и сработает двусторонняя привязка данных
    // поэтому мы избалвяемся сначала от привязки
    this.usersService.setUndefinedLocalUser();
    this.subscription = this.usersService.getUser(id)
      .subscribe({
        next: user => {
          this.user = user;
          this.tmpUser = user
        },
        complete: () => {
          !this.usersService.user && this.usersService.setLocalUser();
        }
      });
  }

  save(): void {
    this.message = undefined;
    if (!this.inputLoginElement.nativeElement.value || !this.inputEmailElement.nativeElement.value ||
      !this.inputBdayElement.nativeElement.value || !this.inputPassportElement.nativeElement.value ||
      !this.inputNameElement.nativeElement.value || !this.inputSurnameElement.nativeElement.value ||
      !this.regexpPassport.test(this.inputPassportElement.nativeElement.value) ||
      !this.regexpEmail.test(this.inputEmailElement.nativeElement.value)) {
      this.emptyLogin = !this.inputLoginElement.nativeElement.value;
      this.emptyEmail = !this.inputEmailElement.nativeElement.value;
      this.emptyBday = !this.inputBdayElement.nativeElement.value;
      this.emptyPassport = !this.inputPassportElement.nativeElement.value;
      this.emptyName = !this.inputNameElement.nativeElement.value;
      this.emptySurname = !this.inputSurnameElement.nativeElement.value;
      this.emptyPassport = !this.regexpPassport.test(this.inputPassportElement.nativeElement.value);
      this.emptyEmail = !this.regexpEmail.test(this.inputEmailElement.nativeElement.value);
      return;
    } else {
      this.usersService.setUndefinedLocalUser();
      this.usersService.updateUser(this.user)
        .subscribe((user) => {
            this.tmpUser = user;
          }, (error: any) => {
            this.message = error.error.response;
            if (this.message == 'login already exist') {
              this.message = "Login already exists";
            }
            if (this.message == 'email already exist') {
              this.message = "Email already exists"
            }
            if (this.message == 'passport already exist') {
              this.message = "Passport already exists"
            }
          },
          () => {
            this.hide_modal();
            this.updateUser.emit(this.tmpUser);
            !this.usersService.user && this.usersService.setLocalUser();
          });
    }
  }

  hide_modal(): void {
    $('#ModalEdit').modal('hide');
  }

}
