import {ClassUsers} from "./ClassUsers";

export class ClassNewUser {
  token: string;
  user: ClassUsers;
}
