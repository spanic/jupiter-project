import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class HandleErrorsService {

  private regexpProfile: RegExp = /^[\w\W]*(profile)[\w\W]*$/;
  private regexpPhoneNumbers: RegExp = /^[\w\W]*(phone-numbers)[\w\W]*$/;
  private regexpJourneys: RegExp = /^[\w\W]*(journeys)[\w\W]*$/;

  constructor(
    private router: Router
  ) { }

  handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      if (error.error.response == "Not authenticated"){
        let date = new Date(0);
        document.cookie = "userid=; path=/; expires=" + date.toUTCString();
        document.cookie = "token=; path=/; expires=" + date.toUTCString();
        const path = document.location.pathname.substring(1);
        if(this.regexpProfile.test(path) || this.regexpJourneys.test(path) || this.regexpPhoneNumbers.test(path) ){
          this.router.navigate([`/log-in`]);
        }
      }
      return of(result as T);
    };
  }
}
