export class ClassRegion {
  regionId: number;
  countryId: number;
  regionName: string;
}
