import {Component, HostListener, OnInit} from '@angular/core';

@Component({
  selector: 'app-accordion',
  templateUrl: './accordion.component.html',
  styleUrls: ['./accordion.component.css', '../app.component.css']
})
export class AccordionComponent implements OnInit {

  constructor() {
  }
  flag: boolean = true;
  ngOnInit() {
  }
  expandArea(): void{
    if(this.flag){
      $('.tmp-cont').addClass('providers-cont');
      $('.tmp-cont').addClass('margin-four');
      $('.content-none').addClass('content-show');
      this.flag = !this.flag;
    }
  }

  @HostListener('window:scroll', ['$event'])
  scrollHandler(event) {
    if (event.path[1].scrollY > 5) {
      $('.tmp-cont').addClass('providers-cont');
      $('.tmp-cont').addClass('margin-four');
      // $('.tmp-cont').addClass('hover');
      $('.content-none').addClass('content-show');
    } else{
      $('.content-none').removeClass('content-show');
      $('.tmp-cont').removeClass('providers-cont');
      $('.tmp-cont').removeClass('margin-four');
      this.flag = true;
    }
  }
}
