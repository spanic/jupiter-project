import { Component, OnInit, OnDestroy } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';

import {UsersService} from '../users.service';
import {CookieService} from "ngx-cookie-service";

import {ClassUsers} from '../ClassUsers';

declare var $ : any;

@Component({
  selector: 'app-model-delete',
  templateUrl: './model-delete.component.html',
  styleUrls: ['./model-delete.component.css', '../app.component.css']
})
export class ModelDeleteComponent implements OnInit, OnDestroy {

  user: ClassUsers;
  subscription: Subscription;

  constructor(
    private usersService: UsersService,
    private router: Router,
    private route: ActivatedRoute,
    private cookie: CookieService
  ) { }

  ngOnInit() {
  }

  ngOnDestroy(){
    !!this.subscription && this.subscription.unsubscribe()
  }

  delete(): void {
    const id = JSON.parse(this.cookie.get('userid'));
    this.subscription = this.usersService.deleteUser(id).subscribe(() => this.onDeleteClick());
  }

  onDeleteClick() {
    let date = new Date(0);
    document.cookie = "userid=; path=/; expires=" + date.toUTCString();
    document.cookie = "token=; path=/; expires=" + date.toUTCString();
    this.usersService.setUndefinedLocalUser();
    $('#deleteModal').modal('hide');
    (this.router.navigate(['/main']));
  }
}
