export class ClassCompletedJourneys {
  id: number;
  userId: number;
  phoneNumber: string;
  tarifId: number;
  balance: number;
}
