import {Component, HostListener, OnInit, OnDestroy} from '@angular/core';
import {Subscription} from 'rxjs';
import {Router} from "@angular/router";

import {CookieService} from "ngx-cookie-service";
import {UsersService} from "../users.service";
import {JourneysService} from '../journeys.service';

import {ClassUsers} from "../ClassUsers";
import {ClassCompletedJourneys} from '../ClassCompletedJourneys';

declare var $: any;

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css', '../app.component.css']
})
export class NavComponent implements OnInit, OnDestroy {

  userAuthId: number;
  isRotated: boolean = false;

  user: ClassUsers;
  loginNav: string;
  journeys: ClassCompletedJourneys[];

  cntUncompleted: number;
  subscriptions: Subscription[] = [];

  constructor(
    public cookie: CookieService,
    public usersService: UsersService,
    public router: Router,
    public journeysService: JourneysService,
  ) {
  }

  ngOnInit() {
    if (this.cookie.get('userid')) {
      this.userAuthId = JSON.parse(this.cookie.get('userid'));
      if (!this.usersService.user) this.usersService.setLocalUser();
      this.getUser();
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(
      (subscription) => !!subscription && subscription.unsubscribe());
    this.subscriptions = [];
  }

  getUser(): void {
    this.subscriptions.push(this.usersService.getUser(this.userAuthId)
      .subscribe({
        next: user => this.user = user,
        complete: () => {
          if (!!this.user) {
            if (this.user.login.length > 12) {
              this.loginNav = `${this.user.login.substring(0, 9)}...`;
            } else {
              this.loginNav = this.user.login;
            }
          }
          this.getCompletedJourneys();
          this.getUncompletedJourneys();
        }
      }));
  }

  getCompletedJourneys(): void {
    const id = JSON.parse(this.cookie.get('userid'));
    this.subscriptions.push(this.journeysService.getCompletedJourneys(id)
      .subscribe({
        next: journeys => this.journeys = journeys,
        complete: () => {
          if (this.journeys && this.journeys.length > 0) {
            // @ts-ignore
            document.styleSheets[0].addRule('#notify:after', 'visibility: visible;');
            // @ts-ignore
            document.styleSheets[0].addRule('#list_notify:after', 'visibility: visible;');
            // @ts-ignore
            document.styleSheets[0].addRule('#notify:after', 'content: "' + this.journeys.length + '";');
            // @ts-ignore
            document.styleSheets[0].addRule('#list_notify:after', 'content: "' + this.journeys.length + '";');
          } else {
            // @ts-ignore
            document.styleSheets[0].addRule('#notify:after', 'visibility: hidden;');
            // @ts-ignore
            document.styleSheets[0].addRule('#list_notify:after', 'visibility: hidden;');
          }
        }
      }));

  }

  getUncompletedJourneys(): void {
    const id = JSON.parse(this.cookie.get('userid'));
    this.subscriptions.push(this.journeysService.getUncompletedJourneys(id)
    // @ts-ignore
      .subscribe({
        next: journeys => this.journeys = journeys,
        complete: () => {
            if (this.journeys) {
              this.cntUncompleted = this.journeys.length;
            }
          }
      }));
  }

  logOut() {
    let date = new Date(0);
    document.cookie = "userid=; path=/; expires=" + date.toUTCString();
    document.cookie = "token=; path=/; expires=" + date.toUTCString();
    this.usersService.setUndefinedLocalUser();
    this.user = undefined;
    this.journeys = undefined;
    $('#deleteModal').modal('hide');
    (this.router.navigate(['/main']));
  }

  Rotate(): void {
    this.isRotated = !this.isRotated;
  }

  @HostListener('document:click', ['$event'])
  public handleClick(event) {
    if (this.isRotated && event.path[0].id != 'arrow') {
      this.isRotated = false;
    }
  }
}
