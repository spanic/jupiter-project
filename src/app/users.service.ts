import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import {MyHttpService} from "./my-http.service";
import {CookieService} from "ngx-cookie-service";
import {HandleErrorsService} from './handle-errors.service';

import {ClassUsers} from './ClassUsers';
import {ClassNewUser} from "./ClassNewUser";

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private usersUrl = 'http://138.197.187.9:8080';
  private _user: any;

  constructor(
    private http: MyHttpService,
    private cookie: CookieService,
    private handleErrors: HandleErrorsService
  ) {
    this.setLocalUser();
  }

  getUser(id: number): Observable<ClassUsers> {
    if(!!this.user) return of(this.user);
    const url = `${this.usersUrl}/user/user/${id}`;
    return this.http.get<ClassUsers>(url)
      .pipe(
        catchError(this.handleErrors.handleError<ClassUsers>(`getUser id=${id}`).bind(this.setUndefinedLocalUser()))
      );
  }
  setLocalUser(): void {
    if (!!this.cookie.get('userid')){
      this.getUser(Number(this.cookie.get('userid'))).subscribe(user => this._user = user);
    }
  }
  setUndefinedLocalUser(): void {
    this._user = undefined;
  }
  get user() {return this._user};

  login(login: string, password: string): Observable<ClassNewUser> {
    const url = `${this.usersUrl}/authentication/signin`;
    return this.http.post<ClassNewUser>(url, {login, password} as ClassUsers)
  }

  addUser(user: ClassUsers): Observable<ClassNewUser> {
    const url = `${this.usersUrl}/authentication/new`;
    return this.http.post<ClassNewUser>(url, user)
  }

  deleteUser(id: number): Observable<ClassUsers> {
    const url = `${this.usersUrl}/user/user/${id}`;
    return this.http.delete<ClassUsers>(url)
      .pipe(
      catchError(this.handleErrors.handleError<ClassUsers>('deleteUser'))
    );
  }

  updateUser(user: ClassUsers): Observable<any> {
    const url = `${this.usersUrl}/user/user`;
    return this.http.put(url, user)
  }
}
