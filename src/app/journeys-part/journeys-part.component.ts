import {Component, OnInit, OnDestroy, ViewChild, ElementRef} from '@angular/core';
import {interval, Subscription} from 'rxjs';
import {startWith, switchMap} from 'rxjs/operators';
import {ActivatedRoute} from '@angular/router';

import {JourneysService} from '../journeys.service';
import {CookieService} from "ngx-cookie-service";

import {ClassJourneys} from '../ClassJourneys';
import {ClassJourneyAndTasks} from '../ClassJourneyAndTasks';
import {ModalEditComponent} from "../modal-edit/modal-edit.component";
import {ModalAddJourneyComponent} from "../modal-add-journey/modal-add-journey.component";

@Component({
  selector: 'app-journeys-part',
  templateUrl: './journeys-part.component.html',
  styleUrls: ['./journeys-part.component.css', '../app.component.css']
})
export class JourneysPartComponent implements OnInit, OnDestroy {
  @ViewChild(ModalAddJourneyComponent) child: ModalAddJourneyComponent;
  journeyAndTasks: ClassJourneyAndTasks[];
  journeyAndTask: ClassJourneyAndTasks;
  id;
  subscriptions: Subscription[] = [];

  constructor(
    private journeysService: JourneysService,
    private route: ActivatedRoute,
    private cookie: CookieService
  ) { }

  ngOnInit() {
    if(!!this.cookie.get('userid')) {
      this.id = JSON.parse(this.cookie.get('userid'));
      this.subscriptions.push(
        interval(3000)
          .pipe(
            startWith(0),
            switchMap(() => this.journeysService.getJourneysTasks(this.id))
          )
          .subscribe(journeyAndTasks => this.journeyAndTasks = journeyAndTasks));
    }
  }

  checkCompletion(journeyAndTask : ClassJourneyAndTasks): boolean {
    return !!(journeyAndTask.infoAboutTasksList.length === 6 && journeyAndTask.infoAboutTasksList[5].journeyTask.taskfinish);
  }

  ngOnDestroy(){
    this.subscriptions.forEach(
      (subscription) => !!subscription && subscription.unsubscribe());
    this.subscriptions = [];
  }

  getJourneysTasks(): void {
    const id = JSON.parse(this.cookie.get('userid'));
    this.subscriptions.push(this.journeysService.getJourneysTasks(id)
      .subscribe(journeyAndTasks => this.journeyAndTasks = journeyAndTasks));
  }

  ContinueJourney(numberId: number, tariffId: number, journeyId: number): void {
    this.subscriptions.push(this.journeysService.ContinueJourney(numberId, tariffId, journeyId)
      .subscribe());
  }

  rechargeButtonAppearing(a: ClassJourneyAndTasks) {
    if (a.infoAboutTasksList.length > 1 && a.infoAboutTasksList[1].journeyTask.taskId == 2
      && a.infoAboutTasksList.length < 3 && a.fullInfoAboutTarif.balance <= 0)
      return true;
  }

  trackByFunction(index, item) {
    if (!item) return null;
    return index;
  }
  modalJourney(): void{
    this.child.getCompletedNumbers();
  }
}
