import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JourneysPartComponent } from './journeys-part.component';

describe('JourneysPartComponent', () => {
  let component: JourneysPartComponent;
  let fixture: ComponentFixture<JourneysPartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JourneysPartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JourneysPartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
