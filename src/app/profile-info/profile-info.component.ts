import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';

import {UsersService} from '../users.service';
import {CookieService} from "ngx-cookie-service";
import {ModalEditComponent} from "../modal-edit/modal-edit.component";

import {ClassUsers} from '../ClassUsers';

declare var $: any;

@Component({
  selector: 'app-profile-info',
  templateUrl: './profile-info.component.html',
  styleUrls: ['./profile-info.component.css']
})
export class ProfileInfoComponent implements OnInit, OnDestroy {

  user: ClassUsers;
  @ViewChild(ModalEditComponent) child: ModalEditComponent;
  modalPassport: string;

  private subscription: Subscription;

  constructor(
    private usersService: UsersService,
    private route: ActivatedRoute,
    private cookie: CookieService,
    private router: Router
  ) {
  }

  ngOnInit() {
    if (!!this.cookie.get('userid')) {
      this.getUser();
      if (!this.usersService.user) this.usersService.setLocalUser();
    } else {
      this.router.navigate(['/log-in']);
    }
  }

  ngOnDestroy() {
    !!this.subscription && this.subscription.unsubscribe()
  }

  getUser(): void {
    const id = Number(this.cookie.get('userid'));
    //TODO: Заменить параметр в запросах. Должен браться не из url, а из кук
    this.subscription = this.usersService.getUser(id)
      .subscribe({
        next: user => this.user = user,
        complete: () => {
          this.modal();
          if (!this.usersService.user) this.usersService.setLocalUser();
        }
      });
  }

  modal(): void {
    if (!!this.user) {
      this.modalPassport = this.user.passport;
      if (!this.modalPassport) {
        this.getModalUser();
        $('#ModalEdit').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#ModalEdit').modal('show');
      }
    }
  }

  getModalUser(): void {
    this.child.message = undefined;
    this.child.getUser();
  }
}
