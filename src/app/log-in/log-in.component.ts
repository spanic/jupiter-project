import {Component, ElementRef, OnInit, OnDestroy, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {Md5} from 'ts-md5';

import {ClassUsers} from '../ClassUsers';
import {UsersService} from '../users.service';
import {ClassNewUser} from "../ClassNewUser";
import {Subscription} from 'rxjs';
import {CookieService} from "ngx-cookie-service";

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css', '../app.component.css']
})
export class LogInComponent implements OnInit, OnDestroy {

  @ViewChild('inputLogin')
  inputLoginElement: ElementRef;

  @ViewChild('inputPassword')
  inputPasswordElement: ElementRef;

  user: ClassUsers;
  newUser: ClassNewUser;

  subscription: Subscription;

  allFields: boolean = true;
  message: string;
  userLoginText: any;
  inputPasswordtext: any;

  constructor(
    private usersService: UsersService,
    private router: Router,
    private cookie: CookieService
  ) {
  }

  ngOnInit() {
    if (!!this.cookie.get('userid')) {
      this.router.navigate([`/profile/${Number(this.cookie.get('userid'))}`]);
    }
  }

  ngOnDestroy() {
    !!this.subscription && this.subscription.unsubscribe()
  }

  login(login: string, password: string): void {
    !!this.usersService.user && this.usersService.setUndefinedLocalUser();
    this.message = undefined;
    if (!login || !password) {
      return;
    }
    this.subscription = this.usersService.login(login, password)
      .subscribe(user => this.newUser = user,
        (error: any) => {
          this.message = error.error.response;
          if (this.message == 'Login not exist') {
            this.message = "Login not exists";
          }
        },
        () => {
          this.usersService.setLocalUser();
          this.user = this.newUser.user;
          let date = new Date(0);
          document.cookie = "userid=; path=/; expires=" + date.toUTCString();
          document.cookie = "token=; path=/; expires=" + date.toUTCString();
          document.cookie = `userid=${this.user.id}; path =/`;
          document.cookie = `token=${this.newUser.token}; path =/`;

          this.router.navigate([`/profile/${this.user.id}`]);
        });
  }

  onLoginClick() {
    if (this.inputLoginElement.nativeElement.value == "" || this.inputPasswordElement.nativeElement.value == "") {
      this.allFields = false;
      this.message = "All fields should be filled";
    } else {
      //хеширование пароля
      const md5 = new Md5();
      let hash = md5.appendStr(this.inputPasswordElement.nativeElement.value).end();
      let password = hash.toString();
      //console.log(hash);

      //отправка пароля(hash) и логина на сервер
      this.login(this.inputLoginElement.nativeElement.value, password);
    }
  }
}
