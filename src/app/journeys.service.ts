import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import {ClassCompletedJourneys} from './ClassCompletedJourneys';
import {MyHttpService} from "./my-http.service";
import {HandleErrorsService} from './handle-errors.service';

import {ClassJourneys} from './ClassJourneys';

@Injectable({
  providedIn: 'root'
})
export class JourneysService {

  private journeysUrl = 'http://138.197.187.9:8080/journey';

  constructor(
    private http: MyHttpService,
    private handleErrors: HandleErrorsService
  ) { }


  getCompletedJourneys(id: number): Observable<ClassCompletedJourneys[]>{
    const url = `${this.journeysUrl}/completedjourneys/${id}`;
    return this.http.get<ClassCompletedJourneys[]>(url)
      .pipe(
        catchError(this.handleErrors.handleError<ClassCompletedJourneys[]>(`getCompletedJourneys id=${id}`))
      );
  }

  getCompletedNumbers(id: number): Observable<ClassCompletedJourneys[]>{
    const url = `${this.journeysUrl}/completed/${id}`;
    return this.http.get<ClassCompletedJourneys[]>(url)
      .pipe(
        catchError(this.handleErrors.handleError<ClassCompletedJourneys[]>(`getCompletedJourneys id=${id}`))
      );
  }

  getUncompletedJourneys(id: number): Observable<ClassCompletedJourneys[]>{
    const url = `${this.journeysUrl}/uncompleted/${id}`;
    return this.http.get<ClassCompletedJourneys[]>(url)
      .pipe(
        catchError(this.handleErrors.handleError<ClassCompletedJourneys[]>(`getUncompletedJourneys id=${id}`))
      );
  }

  addJourney(numberId: number, tariffId: number): Observable<any> {
    const url = `${this.journeysUrl}/new/${numberId}/${tariffId}`;
    return this.http.post<any>(url, {numberId, tariffId}) // /user/user
      .pipe(
        catchError(this.handleErrors.handleError<any>('addJourney'))
      );
  }

  ContinueJourney(numberId: number, tariffId: number, journeyId: number): Observable<any>{
    const url = `${this.journeysUrl}/continue/${numberId}/${tariffId}/${journeyId}`;
    return this.http.post<any>(url, {numberId, tariffId, journeyId})
      .pipe(
        catchError(this.handleErrors.handleError<ClassJourneys>('ContinueJourney'))
      );
  }

  getJourneysTasks(id: number): Observable<any> {
    const url = `${this.journeysUrl}/alltasks/${id}`;
    return this.http.get<any>(url)
      .pipe(
        catchError(this.handleErrors.handleError<any>(`getJourneysTasks id=${id}`))
      );
  }
}
