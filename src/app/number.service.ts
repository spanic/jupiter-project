import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import {MyHttpService} from "./my-http.service";
import {HandleErrorsService} from './handle-errors.service';

import { ClassNumber} from './ClassNumber';

@Injectable({
  providedIn: 'root'
})
export class NumberService {

  private numbersUrl = 'http://138.197.187.9:8080/numbersinfo';

  constructor(
    private http: MyHttpService,
    private handleErrors: HandleErrorsService
  ) { }

  getNumbers(id: number): Observable<ClassNumber[]> {
    const url = `${this.numbersUrl}/fullinfo/${id}`;
    return this.http.get<ClassNumber[]>(url)
      .pipe(
        catchError(this.handleErrors.handleError<ClassNumber[]>(`getNumbers id=${id}`))
      );
  }

  addNumber(phoneNum: any): Observable<any> {
    const url = `${this.numbersUrl}/addnumber`;
    return this.http.post<any>(url, phoneNum)
  }
}
