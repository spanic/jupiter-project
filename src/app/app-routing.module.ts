import { NgModule } from '@angular/core';
import {Routes, RouterModule, PreloadAllModules} from '@angular/router';

import { SignUpComponent } from './sign-up/sign-up.component';
import { LogInComponent } from './log-in/log-in.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { MainComponent } from './main/main.component';
import { AboutComponent } from './about/about.component';
import { TariffsComponent } from './tariffs/tariffs.component';
import { HowToComponent } from './how-to/how-to.component';
import { JourneysComponent} from './journeys/journeys.component';
import {PhoneNumbersComponent} from './phone-numbers/phone-numbers.component';
import {ProfileComponent} from './profile/profile.component';

const routes: Routes = [
  { path: '', redirectTo: '/main', pathMatch: 'full' },
  { path: 'main', component: MainComponent},
  { path: 'log-in', component: LogInComponent},
  { path: 'sign-up', component: SignUpComponent},
  { path: 'reset-password', component: ResetPasswordComponent},
  { path: 'about', component: AboutComponent},
  { path: 'how-to', component: HowToComponent},
  { path: 'tariffs', component: TariffsComponent},
  { path: 'journeys/:id', component: JourneysComponent},
  { path: 'phone-numbers/:id', component: PhoneNumbersComponent},
  { path: 'profile/:id', component: ProfileComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
