import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAddNumberComponent } from './modal-add-number.component';

describe('ModalAddNumberComponent', () => {
  let component: ModalAddNumberComponent;
  let fixture: ComponentFixture<ModalAddNumberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalAddNumberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAddNumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
