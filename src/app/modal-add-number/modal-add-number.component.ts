import {Component, EventEmitter, OnInit, OnDestroy, Output} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';

import {CountriesService} from '../countries.service';
import {ProvidersService} from "../providers.service";
import {TariffsService} from "../tariffs.service";
import {NumberService} from '../number.service';
import {CookieService} from "ngx-cookie-service";


import {ClassCountries} from '../ClassCountries';
import {ClassRegion} from "../ClassRegion";
import {ClassProviders} from "../ClassProviders";
import {ClassTariff} from "../ClassTariff";
import {ClassTariffOfferings} from "../ClassTariffOfferings";
import {ClassNumber} from '../ClassNumber';

declare var $: any;

@Component({
  selector: 'app-modal-add-number',
  templateUrl: './modal-add-number.component.html',
  styleUrls: ['./modal-add-number.component.css', '../app.component.css']
})
export class ModalAddNumberComponent implements OnInit, OnDestroy {

  @Output() public addNumber = new EventEmitter;

  numb: ClassNumber;
  countries: ClassCountries[];
  regions: ClassRegion[];
  providers: ClassProviders[];
  tariffs: ClassTariff[];
  offerings: ClassTariffOfferings[];
  subscriptions: Subscription[] = [];
  message: string;

  selectedCountryId: number;
  selectedRegionId: number;
  selectedProviderId: number;
  selectedTariffId: number;

  constructor(
    private route: ActivatedRoute,
    private countriesService: CountriesService,
    private providersService: ProvidersService,
    private tariffsService: TariffsService,
    private numberService: NumberService,
    private cookie: CookieService
  ) {
  }

  ngOnInit() {
    this.getCountries();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(
      (subscription) => !!subscription && subscription.unsubscribe());
    this.subscriptions = [];
  }

  getCountries(): void {
    this.subscriptions.push(this.countriesService.getCountries()
      .subscribe(countries => this.countries = countries));
  }

  getRegions(): void {
    this.subscriptions.push(this.countriesService.getRegions(this.selectedCountryId)
      .subscribe(regions => this.regions = regions));
    this.selectedRegionId = undefined;
    this.selectedProviderId = undefined;
    this.providers = [];
    this.selectedTariffId = undefined;
    this.tariffs = [];
    this.offerings = [];
  }

  getProviders(): void {
    this.subscriptions.push(this.providersService.getProviders(this.selectedRegionId)
      .subscribe(providers => this.providers = providers));
    this.selectedProviderId = undefined;
    this.selectedTariffId = undefined;
    this.tariffs = [];
    this.offerings = [];
  }

  getTariffs(): void {
    this.subscriptions.push(this.tariffsService.getTariffs(this.selectedRegionId, this.selectedProviderId)
      .subscribe(tariffs => this.tariffs = tariffs));
    this.selectedTariffId = undefined;
    this.offerings = [];
  }

  getTariffOfferings(): void {
    this.subscriptions.push(this.tariffsService.getTariffOfferings(this.selectedTariffId)
      .subscribe(offerings => this.offerings = offerings));
  }

  add(phoneNumber: string, tarifId: number): void {
    this.message = undefined;
    const userId = JSON.parse(this.cookie.get('userid'));
    phoneNumber = phoneNumber.trim();
    if (!phoneNumber && !tarifId) {
      return;
    }
    this.subscriptions.push(this.numberService.addNumber({phoneNumber, userId, tarifId})
      .subscribe(numb => this.numb = numb,
        (error: any) => {
          this.message = error.error.response;
          if (this.message == 'number already exist') {
            this.message = "Number already exists";
          }
        },
        () => {
          this.addNumber.emit();
          $('#ModalAddNumber').modal('hide');
        }));
  }

  hide_modal(): void {
    this.message = undefined;
    $('#ModalAddNumber').modal('hide');
  }
}
