import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';

import {CountriesService} from '../countries.service';
import {ProvidersService} from '../providers.service';
import {TariffsService} from '../tariffs.service';
import {OfferingsService} from '../offerings.service';

import {ClassOfferings} from '../ClassOfferings';
import {ClassCountries} from '../ClassCountries';
import {ClassRegion} from '../ClassRegion';
import {ClassProviders} from '../ClassProviders';
import {ClassTariff} from '../ClassTariff';
import {ClassTariffOfferings} from '../ClassTariffOfferings';

@Component({
  selector: 'app-tariffs',
  templateUrl: './tariffs.component.html',
  styleUrls: ['./tariffs.component.css', '../app.component.css']
})
export class TariffsComponent implements OnInit, OnDestroy {

  countries: ClassCountries[];
  regions: ClassRegion[];
  providers: ClassProviders[];
  tariffs: ClassTariff[];
  offerings: ClassTariffOfferings[];
  offering: ClassOfferings[];
  subscriptions: Subscription[] = [];

  selectedCountryId: number;
  selectedRegionId: number;
  selectedProviderId: number;
  selectedTariffId: number;

  constructor(
    public route: ActivatedRoute,
    public countriesService: CountriesService,
    public providersService: ProvidersService,
    public tariffsService: TariffsService,
    public offeringsService: OfferingsService
  ) {
  }

  ngOnInit() {
    this.getCountries();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(
      (subscription) => !!subscription && subscription.unsubscribe());
    this.subscriptions = [];
  }

  getCountries(): void {
    this.subscriptions.push(this.countriesService.getCountries()
      .subscribe(countries => this.countries = countries));
  }

  getRegions(): void {
    this.subscriptions.push(this.countriesService.getRegions(this.selectedCountryId)
      .subscribe(regions => this.regions = regions));
    this.selectedRegionId = undefined;
    this.selectedProviderId = undefined;
    this.providers = [];
    this.selectedTariffId = undefined;
    this.tariffs = [];
    this.offerings = [];
  }

  getProviders(): void {
    this.subscriptions.push(this.providersService.getProviders(this.selectedRegionId)
      .subscribe(providers => this.providers = providers));
    this.selectedProviderId = undefined;
    this.selectedTariffId = undefined;
    this.tariffs = [];
    this.offerings = [];
  }

  getTariffs(id: number): void {
    this.subscriptions.push(this.tariffsService.getTariffs(this.selectedRegionId, id)
      .subscribe(tariffs => this.tariffs = tariffs));
    this.selectedTariffId = undefined;
    this.offerings = [];
    console.log(this.selectedRegionId);
    console.log(id);
  }

  getOffering(id: number): void {
    this.subscriptions.push(this.offeringsService.getOfferings(id)
      .subscribe(offering => this.offering = offering));
  }
}
