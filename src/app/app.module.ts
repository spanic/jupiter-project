import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatNativeDateModule} from '@angular/material';
import {MatProgressBarModule} from '@angular/material';
import {MatInputModule} from '@angular/material';
import {MatFormFieldModule} from '@angular/material/form-field';
import {IConfig, NgxMaskModule} from 'ngx-mask';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { CaruselComponent } from './carusel/carusel.component';
import { AccordionComponent } from './accordion/accordion.component';
import { MainComponent } from './main/main.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { LogInComponent } from './log-in/log-in.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { AboutComponent } from './about/about.component';
import { TariffsComponent } from './tariffs/tariffs.component';
import { HowToComponent } from './how-to/how-to.component';
import { FooterComponent } from './footer/footer.component';
import { AccountNavComponent } from './account-nav/account-nav.component';
import { ProfileInfoComponent } from './profile-info/profile-info.component';
import { NumbersPartComponent } from './numbers-part/numbers-part.component';
import { ModalAddNumberComponent } from './modal-add-number/modal-add-number.component';
import { ModalEditComponent } from './modal-edit/modal-edit.component';
import { ProfileComponent } from './profile/profile.component';
import { PhoneNumbersComponent } from './phone-numbers/phone-numbers.component';
import { JourneysComponent } from './journeys/journeys.component';
import { JourneysPartComponent } from './journeys-part/journeys-part.component';
import { ProfileAccDeleteComponent } from './profile-acc-delete/profile-acc-delete.component';
import {CookieService} from "ngx-cookie-service"
import * as $ from 'jquery';

import {bcrypt} from 'bcrypt';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ModelDeleteComponent } from './model-delete/model-delete.component';
import { ModalAddJourneyComponent } from './modal-add-journey/modal-add-journey.component';
import {MyHttpService} from "./my-http.service";
import { OopsComponent } from './oops/oops.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    CaruselComponent,
    AccordionComponent,
    MainComponent,
    SignUpComponent,
    LogInComponent,
    ResetPasswordComponent,
    AboutComponent,
    TariffsComponent,
    HowToComponent,
    FooterComponent,
    AccountNavComponent,
    ProfileInfoComponent,
    FooterComponent,
    NumbersPartComponent,
    ModalAddNumberComponent,
    ModalEditComponent,
    AccountNavComponent,
    ProfileComponent,
    PhoneNumbersComponent,
    JourneysComponent,
    JourneysPartComponent,
    ProfileAccDeleteComponent,
    ModelDeleteComponent,
    ModalAddJourneyComponent,
    OopsComponent
  ],
  imports: [
    NgxMaskModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatProgressBarModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatInputModule
  ],
  entryComponents: [],
  providers: [CookieService, MyHttpService, HttpClientModule],
  bootstrap: [AppComponent]
})
export class AppModule { }

