import {ClassFullInfoAboutTarif} from './ClassFullInfoAboutTarif';
import {ClassTaskAndName} from './ClassTaskAndName';

export class ClassJourneyAndTasks {
  fullInfoAboutTarif: ClassFullInfoAboutTarif;
  infoAboutTasksList: ClassTaskAndName[];
}
