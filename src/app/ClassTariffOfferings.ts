export class ClassTariffOfferings {
  tariffId: number;
  offeringId: number;
  offeringPrice: string;
  quantity: string;
  offeringName: string;
  offeringDesc: string;
}
