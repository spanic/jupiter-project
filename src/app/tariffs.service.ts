import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import {catchError} from "rxjs/operators";

import {MyHttpService} from "./my-http.service";
import {HandleErrorsService} from './handle-errors.service';

import {ClassTariff} from "./ClassTariff";
import {ClassTariffOfferings} from "./ClassTariffOfferings";

@Injectable({
  providedIn: 'root'
})
export class TariffsService {

  private offeringsUrl = 'http://138.197.187.9:8080/tarif';

  constructor(
    private http: MyHttpService,
    private handleErrors: HandleErrorsService
  ) { }

  getTariffs(regionId: number, providerId: number): Observable<ClassTariff[]> {
    const url = `${this.offeringsUrl}/price/${regionId}/${providerId}`;
    return this.http.get<ClassTariff[]>(url)
      .pipe(
        catchError(this.handleErrors.handleError<ClassTariff[]>('getTariffs', []))
      );
  }

  getTariffOfferings(id: number): Observable<ClassTariffOfferings[]> {
    const url = `${this.offeringsUrl}foffers/${id}`;
    return this.http.get<ClassTariffOfferings[]>(url)
      .pipe(
        catchError(this.handleErrors.handleError<ClassTariffOfferings[]>('getTariffOffers', []))
      );
  }
}
