import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {catchError} from 'rxjs/operators';

import {MyHttpService} from "./my-http.service";
import {HandleErrorsService} from './handle-errors.service';

import {ClassProviders} from "./ClassProviders";
import {ClassOfferings} from './ClassOfferings';

@Injectable({
  providedIn: 'root'
})
export class ProvidersService {

  private providersUrl = 'http://138.197.187.9:8080/region/providersForRegion';
  constructor(
    private http: MyHttpService,
    private handleErrors: HandleErrorsService
  ) { }

  getProviders(id: number): Observable<ClassProviders[]> {
    const url = `${this.providersUrl}/${id}`;
    return this.http.get<ClassProviders[]>(url)
    .pipe(
        catchError(this.handleErrors.handleError<ClassOfferings[]>(`getOfferings`))
      );
  }
}
