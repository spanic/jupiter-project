import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CookieService} from 'ngx-cookie-service';

@Injectable()
export class MyHttpService {

  constructor(private httpClient: HttpClient,  private cookie: CookieService) { }

  get<T>(url: string): Observable<any> {
    const headers = this.createHeaders();
    return this.httpClient.get<T>(url, {headers});
  }
  post<T>(url: string, body: any): Observable<any> {
    const headers = this.createHeaders();
    return this.httpClient.post<T>(url, body, {headers});
  }
  put<T>(url: string, body: any): Observable<any> {
    const headers = this.createHeaders();
    return this.httpClient.put<T>(url, body, {headers});
  }
  delete<T>(url: string): Observable<any> {
    const headers = this.createHeaders();
    return this.httpClient.delete<T>(url, {headers});
  }
  private createHeaders(): any {
    let  headers = new  HttpHeaders().set('Content-Type', 'application/json');
    if (this.cookie.get('token') && this.cookie.get('userid')) {
      headers = headers
        .append('token', this.cookie.get('token'))
        .append('userid', this.cookie.get('userid'));
    }
    return headers;
  }
}
