export class ClassFullInfoAboutTarif {
  date: Date;
  endDate: Date;
  balance: number;
  journeyId: number;
  newTariffId: number;
  nextProvider: string;
  nextTarif: string;
  number: string;
  numberId: number;
  previousProvider: string;
  previousTarif: string;
}
